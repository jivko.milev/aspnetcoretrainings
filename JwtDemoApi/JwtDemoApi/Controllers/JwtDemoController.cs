﻿using JwtDemoApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JwtDemoApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "user, admin")]
    public class JwtDemoController : ControllerBase
    {
        private readonly ISecurityService _securityService;

        public JwtDemoController(ISecurityService securityService) 
        {
            _securityService = securityService;
        }

        [HttpGet("login")]
        [AllowAnonymous]
        public IActionResult Login(string username)
        {
            var token = _securityService.GenerateJwtToken(username);

            return Ok(token);
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("You have access to the get method.");
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public IActionResult Post()
        {
            return Ok("You have access to the post method.");
        }
    }
}
