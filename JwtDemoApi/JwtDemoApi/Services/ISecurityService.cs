﻿namespace JwtDemoApi.Services
{
    public interface ISecurityService
    {
        string GenerateJwtToken(string username);
    }
}
